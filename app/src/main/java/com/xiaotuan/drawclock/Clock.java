package com.xiaotuan.drawclock;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Calendar;
import java.util.TimeZone;

public class Clock extends View {
    private static final String TAG = "Clock";

    // 阿拉伯数字字符串数组
    private static final String[] ARABIC_DIGITALS = {"12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
    // 罗马数字字符串数组
    private static final String[] ROMAN_DIGITALS = {"XII", "Ⅰ","Ⅱ", "Ⅲ", "Ⅳ", "Ⅴ", "Ⅵ", "Ⅶ", "Ⅷ", "Ⅸ", "Ⅹ", "Ⅺ"};
    private static final int ARABIC_STYLE = 0;
    private static final int ROMAN_STYLE = 1;
    private static final int UPDATE_TIME_DELAYED = 100;

    private Paint mCardinalPaint;
    private Paint mTextPaint;
    private Paint mCirclePaint;
    private Paint mCenterCirclePaint;
    private Paint mHourHandPaint;
    private Paint mMinuteHandPaint;
    private Paint mSecondHandPaint;
    private Paint mFramePaint;
    private Calendar mCurrentTime;
    private Handler mHander = new Handler();

    private Typeface mTextStyle;
    private String mTimeZone;
    private int mTextSize;
    private int mDigitalStyle;
    private int mFrameWidth;
    private int mCalibrationWidth;
    private int mCalibrationNormalLength;
    private int mCalibrationHalfLength;
    private int mCalibrationWholeLength;
    private int mHourHandWidth;
    private int mMinuteHandWidth;
    private int mSecondHandWidth;
    private int mTextColor;
    private int mFrameColor;
    private int mCalibrationColor;
    private int mHourHandColor;
    private int mMinuteHandColor;
    private int mSecondHandColor;
    private int mBackgroundColor;
    private int mCenterCircleColor;
    private int mCenterCircleRadiu;
    private boolean mEnableFrame;

    private int mTextHeight;

    public Clock(Context context) {
        this(context, null);
    }

    public Clock(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Clock(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    @SuppressLint("ResourceType")
    public Clock(Context context, AttributeSet attrs, int defStyleAttr, int defStyle) {
        super(context, attrs, defStyleAttr);
        Log.d(TAG, "Clock()...");
        Resources res = getResources();
        // 获取样式资源
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Clock, defStyleAttr, defStyle);
        mBackgroundColor = a.getColor(R.styleable.Clock_backgroundColor, res.getColor(R.color.default_clock_background_color));
        mTextSize = a.getDimensionPixelSize(R.styleable.Clock_textSize, res.getDimensionPixelSize(R.dimen.default_clock_text_size));
        mTextColor = a.getColor(R.styleable.Clock_textColor, res.getColor(R.color.default_clock_text_color));
        mDigitalStyle = a.getInt(R.styleable.Clock_digitalStyle, res.getInteger(R.integer.default_clock_digital_style));
        mEnableFrame = a.getBoolean(R.styleable.Clock_enableFrame, res.getBoolean(R.bool.default_clock_enabled_frame));
        mFrameWidth = a.getDimensionPixelSize(R.styleable.Clock_frameWidth, res.getDimensionPixelSize(R.dimen.default_clock_frame_width));
        mFrameColor = a.getColor(R.styleable.Clock_frameColor, res.getColor(R.color.default_clock_frame_color));
        mCalibrationColor = a.getColor(R.styleable.Clock_calibrationColor, res.getColor(R.color.default_clock_calibration_color));
        mCalibrationWidth = a.getDimensionPixelSize(R.styleable.Clock_calibrationWidth, res.getDimensionPixelSize(R.dimen.default_clock_calibration_width));
        mCalibrationNormalLength = a.getDimensionPixelSize(R.styleable.Clock_calibrationNormalLength, res.getDimensionPixelSize(R.dimen.default_clock_calibration_normal_length));
        mCalibrationHalfLength = a.getDimensionPixelSize(R.styleable.Clock_calibrationHalfLength, res.getDimensionPixelSize(R.dimen.default_clock_calibration_half_length));
        mCalibrationWholeLength = a.getDimensionPixelSize(R.styleable.Clock_calibrationWholeLength, res.getDimensionPixelSize(R.dimen.default_clock_calibration_whole_length));
        mHourHandColor = a.getColor(R.styleable.Clock_hourHandColor, res.getColor(R.color.default_clock_hour_hand_color));
        mHourHandWidth = a.getDimensionPixelSize(R.styleable.Clock_hourHandWidth, res.getDimensionPixelSize(R.dimen.default_clock_hour_hand_width));
        mMinuteHandColor = a.getColor(R.styleable.Clock_minuteHandColor, res.getColor(R.color.default_minute_hand_color));
        mMinuteHandWidth = a.getDimensionPixelSize(R.styleable.Clock_minuteHandWidth, res.getDimensionPixelSize(R.dimen.default_minute_hand_width));
        mSecondHandColor = a.getColor(R.styleable.Clock_secondHandColor, res.getColor(R.color.default_second_hand_color));
        mSecondHandWidth = a.getDimensionPixelSize(R.styleable.Clock_secondHandWidth, res.getDimensionPixelSize(R.dimen.default_second_hand_width));
        mCenterCircleColor = a.getColor(R.styleable.Clock_centerCircleColor, res.getColor(R.color.default_clock_center_circle_color));
        mCenterCircleRadiu = a.getDimensionPixelSize(R.styleable.Clock_centerCircleRadiu, res.getDimensionPixelSize(R.dimen.default_clock_center_circle_radiu));
        mTimeZone = a.getString(R.styleable.Clock_timeZone);
        int style = a.getInt(R.styleable.Clock_textStyle, res.getInteger(R.integer.default_clock_text_style));
        a.recycle();
        switch (style) {
            case 1:
                mTextStyle = Typeface.DEFAULT_BOLD;
                break;

            case 2:
                mTextStyle = Typeface.MONOSPACE;

            case 3:
                mTextStyle = Typeface.SERIF;
                break;

            case 4:
                mTextStyle = Typeface.SANS_SERIF;
                break;

            default:
                mTextStyle = Typeface.DEFAULT;
        }

        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(mBackgroundColor);
        mCirclePaint.setStrokeWidth(1);
        mCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(mTextColor);
        mTextPaint.setTypeface(mTextStyle);
        mTextPaint.setTextSize(mTextSize);

        mTextHeight = (int) mTextPaint.measureText("yY");

        mCardinalPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCardinalPaint.setColor(mCalibrationColor);
        mCardinalPaint.setStrokeWidth(mCalibrationWidth);
        mCardinalPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mHourHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHourHandPaint.setColor(mHourHandColor);
        mHourHandPaint.setStrokeWidth(mHourHandWidth);
        mHourHandPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mMinuteHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMinuteHandPaint.setColor(mMinuteHandColor);
        mMinuteHandPaint.setStrokeWidth(mMinuteHandWidth);
        mMinuteHandPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mSecondHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSecondHandPaint.setColor(mSecondHandColor);
        mSecondHandPaint.setStrokeWidth(mSecondHandWidth);
        mSecondHandPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mFramePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFramePaint.setColor(mFrameColor);
        mFramePaint.setStrokeWidth(mFrameWidth);
        mFramePaint.setStyle(Paint.Style.STROKE);

        mCenterCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCenterCirclePaint.setColor(mCenterCircleColor);
        mCenterCirclePaint.setStrokeWidth(1);
        mCenterCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        // 如果设置时区，则对Calendar设置时区
        mCurrentTime = Calendar.getInstance();
        if (!TextUtils.isEmpty(mTimeZone)) {
            mCurrentTime.setTimeZone(TimeZone.getTimeZone(mTimeZone));
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // View已经加载到窗口上，开始启动更新时间线程
        updateTime();
        // 注册时间、时区改变广播
        registerTimeChangeReceiver();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        // View已经离开窗口，停止更新时间线程
        updateTime();
        // 取消时间、时区广播监听
        unregisterTimeChangeReceiver();
    }

    /**
     * 设置时钟时区
     *
     * @param timeZone 时区字符串，格式为GMT+8:00。如果为null，则取消设置时区
     */
    private void setTimeZone(String timeZone) {
        stopUpdateTime();
        mCurrentTime = Calendar.getInstance();
        mTimeZone = timeZone;
        if (!TextUtils.isEmpty(mTimeZone)) {
            mCurrentTime.setTimeZone(TimeZone.getTimeZone(mTimeZone));
        }
        postInvalidate();
        updateTime();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredWidth = measure(widthMeasureSpec);
        int measuredHeight = measure(heightMeasureSpec);

        int d = Math.min(measuredWidth, measuredHeight);

        setMeasuredDimension(d, d);
    }

    private int measure(int measureSpec) {
        int result = 0;

        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.UNSPECIFIED) {
            result = 200;
        } else {
            result = specSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();

        int px = measuredWidth / 2;
        int py = measuredHeight / 2;

        int radius = Math.min(px, py);

        // 绘制时钟边框
        if (mEnableFrame) {
            canvas.drawCircle(px, py, radius, mFramePaint);
            radius -= mFrameWidth;
        }

        // 绘制时钟背景
        canvas.drawCircle(px, py, radius, mCirclePaint);

        int textWidth = (int) mTextPaint.measureText("W");
        int cardinalX = px - textWidth / 2;
        int cardinalY = py - radius + mTextHeight;

        // 每15°绘制一个标记，每45°绘制一个文本
        for (int i = 0; i < 60; i++) {
            // 绘制一个标记
            if (i % 5 == 0) {
                canvas.drawLine(px, py - radius, px, py - radius + mCalibrationWholeLength, mCardinalPaint);
            } else {
                canvas.drawLine(px, py - radius, px, py - radius + mCalibrationNormalLength, mCardinalPaint);
            }

            canvas.save();

            canvas.translate(0, mTextHeight / 2);

            if (i % 5 == 0) {
                //绘制数字字符
                String dirString = getDigitalString(i);
                textWidth = (int) mTextPaint.measureText(dirString);
                cardinalX = px - textWidth / 2;
                cardinalY = py - radius + mTextHeight;
                if (i == 20 || i == 25 || i == 30 || i == 35 || i == 40) {
                    // 绘制4、5、6、7、8点，这些字符需要翻转
                    canvas.rotate(180, cardinalX, cardinalY);
                    canvas.drawText(dirString, cardinalX - textWidth, cardinalY + mTextHeight * 3 / 4, mTextPaint);
                    canvas.rotate(-180, cardinalX,cardinalY);
                } else {
                    // 绘制1、2、3、9、10、11、12点，这些字符不需要翻转
                    canvas.drawText(dirString, cardinalX, cardinalY, mTextPaint);
                }
            }

            canvas.restore();
            // 每次旋转3°
            canvas.rotate(6, px, py);
        }
        // 绘制中心圆
        canvas.drawCircle(px, py, mCenterCircleRadiu, mCenterCirclePaint);
        // 绘制时针
        canvas.save();
        canvas.rotate(getHourDegrees(), px, py);
        canvas.drawRect(px - 5, py - radius + mCalibrationWholeLength * 6, px + 5, py + radius / 10, mHourHandPaint);
        canvas.restore();
        // 绘制分针
        canvas.save();
        canvas.rotate(getMinuteDegrees(), px, py);
        canvas.drawRect(px - 2, py - radius + mCalibrationWholeLength * 4, px + 2, py + radius / 10, mMinuteHandPaint);
        canvas.restore();
        // 绘制秒针
        canvas.save();
        canvas.rotate(getSecondDegrees(), px, py);
        canvas.drawRect(px - 2, py - radius + mCalibrationNormalLength * 3 / 2, px + 2, py + radius / 10, mSecondHandPaint);
        canvas.restore();
    }

    /**
     * 获取时针旋转的角度
     * @return 返回时针旋转的角度
     */
    private float getHourDegrees() {
        int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mCurrentTime.get(Calendar.MINUTE);
        int second = mCurrentTime.get(Calendar.SECOND);
        return hour * 30 + (minute / 60.0f) * 30 + (second / 3600.0f) * 30;
    }

    /**
     * 获取分针旋转的角度
     * @return 返回分针旋转的角度
     */
    private float getMinuteDegrees() {
        int minute = mCurrentTime.get(Calendar.MINUTE);
        int second = mCurrentTime.get(Calendar.SECOND);
        return minute * 6 + (second / 60.0f) * 6;
    }

    /**
     * 获取秒针旋转的角度
     * @return 返回秒针旋转的角度
     */
    private float getSecondDegrees() {
        int second = mCurrentTime.get(Calendar.SECOND);
        return second * 6;
    }

    /**
     * 获取时钟数字字符
     * @param index 数字字符的位置
     * @return 返回时钟数字字符
     */
    private String getDigitalString(int index) {
        String dirString = "";
        switch (index) {
            case 5:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[1];
                } else {
                    dirString = ROMAN_DIGITALS[1];
                }
                break;

            case 10:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[2];
                } else {
                    dirString = ROMAN_DIGITALS[2];
                }
                break;

            case 15:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[3];
                } else {
                    dirString = ROMAN_DIGITALS[3];
                }
                break;

            case 20:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[4];
                } else {
                    dirString = ROMAN_DIGITALS[4];
                }
                break;

            case 25:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[5];
                } else {
                    dirString = ROMAN_DIGITALS[5];
                }
                break;

            case 30:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[6];
                } else {
                    dirString = ROMAN_DIGITALS[6];
                }
                break;

            case 35:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[7];
                } else {
                    dirString = ROMAN_DIGITALS[7];
                }
                break;

            case 40:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[8];
                } else {
                    dirString = ROMAN_DIGITALS[8];
                }
                break;

            case 45:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[9];
                } else {
                    dirString = ROMAN_DIGITALS[9];
                }
                break;

            case 50:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[10];
                } else {
                    dirString = ROMAN_DIGITALS[10];
                }
                break;

            case 55:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[11];
                } else {
                    dirString = ROMAN_DIGITALS[11];
                }
                break;

            default:
                if (mDigitalStyle == ARABIC_STYLE) {
                    dirString = ARABIC_DIGITALS[0];
                } else {
                    dirString = ROMAN_DIGITALS[0];
                }
        }

        return dirString;
    }

    /**
     * 启动更新时间线程
     */
    private void updateTime() {
        mHander.removeCallbacks(updateTimeRunnable);
        mHander.postDelayed(updateTimeRunnable, UPDATE_TIME_DELAYED);
    }

    /**
     * 停止更新时间线程
     */
    private void stopUpdateTime() {
        mHander.removeCallbacks(updateTimeRunnable);
    }

    /**
     * 更新时间Runnable
     */
    private Runnable updateTimeRunnable = new Runnable() {
        @Override
        public void run() {
            mCurrentTime = Calendar.getInstance();
            if (!TextUtils.isEmpty(mTimeZone)) {
                mCurrentTime.setTimeZone(TimeZone.getTimeZone(mTimeZone));
            }
            postInvalidate();
            mHander.postDelayed(updateTimeRunnable, UPDATE_TIME_DELAYED);
        }
    };

    /**
     * 注册时间、时区改变监听广播
     */
    private void registerTimeChangeReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        getContext().registerReceiver(mTimeChangeReceiver, filter);
    }

    /**
     * 取消注册时间、时区改变监听广播
     */
    private void unregisterTimeChangeReceiver() {
        getContext().unregisterReceiver(mTimeChangeReceiver);
    }

    /**
     * 时间、时区改变监听广播
     */
    private BroadcastReceiver mTimeChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopUpdateTime();
            mCurrentTime = Calendar.getInstance();
            if (!TextUtils.isEmpty(mTimeZone)) {
                mCurrentTime.setTimeZone(TimeZone.getTimeZone(mTimeZone));
            }
            postInvalidate();
            updateTime();
        }
    };
}
